﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    AudioSource _audioSource;

    [SerializeField]
    AudioClip _audioStart;

    [SerializeField]
    PACMANMovement _pacman;

    [SerializeField]
    GameObject _GameOverUI;


    bool _isGamingReady = false;

    public bool IsGameReady()
    {
        return _isGamingReady;
    }




    // Use this for initialization
    void Start()
    {
        if (_pacman == null)
            throw new UnityException("PacMan is missing!");

        _GameOverUI.SetActive(false);

        StartCoroutine(CheckStarMusic());
    }

    void Awake()
    {

        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckDefeat())
        {
            OnDefeat();
        }
        else if (CheckVictory())
        {

            OnVictory();

        }

    }

    void OnVictory()
    {
        Debug.Log("HAI VINTO!");

        SceneManager.LoadScene("stage02");
    }

    void OnDefeat()
    {
        _GameOverUI.SetActive(true);
        _isGamingReady = false;


    }

    /// <summary>
    /// Controlla la condizione di vittoria
    /// </summary>
    /// <returns> True se il giocatore ha vinto, altrimenti false.</returns>

    bool CheckVictory()
    {
        return (_pacman.GetEatenPills() == Pill.GetTotalPillCount());

    }
    /// <summary>
    /// Controlla le condizioni di sconfitta. (Quando il giocatore arriva a 0 vite)
    /// </summary>
    /// <returns></returns>
    bool CheckDefeat()
    {

        return (_pacman.GetLives() == 0);

    }

    /// <summary>
    /// Aspetta che la musica di inizio sia terminata prima di permettere l'avvio del gioco.
    /// </summary>
    /// <returns></returns>
    IEnumerator CheckStarMusic()
    {
        _isGamingReady = false;
        _audioSource.PlayOneShot(_audioStart);
        while (_audioSource.isPlaying)
            yield return null;
        _isGamingReady = true;


    }

    public void RestartLivel1()
    {
        SceneManager.LoadScene("stage01");
    }

}
