﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PACMANMovement : MonoBehaviour
{
    [SerializeField]
    AudioClip _audioMov;

    [SerializeField]
    AudioClip _audioDeath;

    [SerializeField]
    float MovementSpeed;

    public int TotalPoints;

    int Lives = 3;

    AudioSource _audioSource;

    GameManager _gameManager;


    public int GetLives()
    {

        return Lives;
    }

    /// <summary>
    /// ha o meno il power up.
    /// </summary>
    bool _hasPowerup;

    /// <summary>
    /// Tempo trascorso da quando si è raccolto il power-up
    /// </summary>
    float _powerUpElapseTime = 0;

    /// <summary>
    /// Indica la durata di tempo trascorso
    /// </summary>
    float _PowerUpDuration = 10;

    /// <summary>
    /// Rappresenta il numero di fantasmi mangiati all' interno del power-up 
    /// </summary>
    int _eatenGhosts;
    
    /// <summary>
    /// Rappresenta in conteggio delle pillole mangiate
    /// </summary>

    int _eatenPills;

    public int GetEatenPills()
    {
        return _eatenPills;

    }


    void Awake()
    {
        _gameManager = GameObject.FindObjectOfType<GameManager>();
        _audioSource = GetComponent<AudioSource>();

    }



    // Use this for initialization
    void Start()
    {
       
    }

    /// <summary>
    /// Aspetta che la musica abbia finito prima di iniziare la partita
    /// </summary>

    // Update is called once per frame
    void Update()
    {
        if (_gameManager.IsGameReady() == false)
            return;



        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        //Debug.Log("h:" + h + " - v:" + v );
        if (h != 0)
        {
            transform.Translate(Vector3.right * MovementSpeed * h);

        }
        else
        {
            transform.Translate(Vector3.forward * MovementSpeed * v);

        }

        if (h != 0 || v != 0)
        {
            if (!_audioSource.isPlaying)


                //_audioSource.Stop():
                _audioSource.PlayOneShot(_audioMov);


        }


        if (_hasPowerup)
        {
            _powerUpElapseTime += Time.deltaTime;
            //Debug.Log("ELAPSED TIME" + _powerUpElapseTime);

            if (_powerUpElapseTime >= _PowerUpDuration)
            {
                _hasPowerup = false;
                _eatenGhosts = 0; 

            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("GNAM!");

        if (other.gameObject.tag == "pill" || other.gameObject.tag == "power up")
        {
            OnEatPill(other);
        }

        if (other.gameObject.tag == "ghost")
        {
            if (!_hasPowerup)
            {
                OnHit();
            }


            else
            {
                OnEatGhost(other);


            }
        }

    }

    /// <summary>
    /// Codice eseguito quando PacMan mangia un fantasma.
    /// </summary>
    /// <param name="other"></param>
    private void OnEatGhost(Collider other)
    {
        if (other == null)
            throw new System.ArgumentNullException("other", "other cannot be null");

        _eatenGhosts++;

        TotalPoints += (int)Mathf.Pow(2, _eatenGhosts - 1) * Ghost.Points;
        //Destroy(other.gameObject);
        other.gameObject.SendMessage("OnDeath", SendMessageOptions.RequireReceiver );

    }

    /// <summary>
    /// Codice eseguito quando si mangia la pillola.
    /// </summary>
    /// <param name="other"></param>
    void OnEatPill(Collider other)
    {
        Debug.Log("GNAM!");

        _eatenPills++;

        Pill pill = other.gameObject.GetComponent<Pill>();
        TotalPoints += pill.Points;



        if (pill is PowerUp)
        {
            Debug.Log("POWER UP!!!");
            _hasPowerup = true;
            _powerUpElapseTime = 0;

        }

        Destroy(other.gameObject);

        //if (_eatenPills == Pill.GetTotalPillCount());
        // Debug.Log("HAI VINTO");
    }

    /// <summary>
    ///  Codice eseguito quando PacMan viene colpito da un fantasma
    /// </summary>
    void OnHit()
    {


        if (!_audioSource.isPlaying)
            _audioSource.PlayOneShot(_audioDeath);

        Lives -= 1;

    }

}
