﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost : MonoBehaviour
{
    GameManager _gameManager;
    NavMeshAgent _navAgent;
    GameObject _player;
    PACMANMovement _pacman;

    [SerializeField]
    GameObject GhostMesh;

    [SerializeField]
    GameObject EyesMesh;

    [SerializeField]
    Transform SpawnPoint;

    static public int Points = 200;

    bool _isDead;

    void Awake()
    {
        _gameManager = GameObject.FindObjectOfType<GameManager>();
        _navAgent = GetComponent<NavMeshAgent>();

    }



    // Use this for initialization

    private void OnDisable()
    {
        Destroy(GhostMesh);

    }

    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        if (_player == null)
            throw new UnityException("Player missing");

        if (SpawnPoint == null)
            throw new UnityException("Spawn point missing");

        _pacman = _player.GetComponent<PACMANMovement>();
        EyesMesh.SetActive(false);

        //_navAgent.SetDestination(_player.transform.position);
        Debug.Log("Totale pillole" + Pill.GetTotalPillCount());

    }

    // Update is called once per frame
    void Update()
    {
        if (_gameManager.IsGameReady() == false)
            return;
        //_player = GameObject.FindGameObjectsWithTag("Player");
        GhostMesh.transform.position = transform.position;
        EyesMesh.transform.position = transform.position;
        _navAgent.SetDestination(_player.transform.position);

        if (_isDead == false)
            _navAgent.SetDestination(_player.transform.position);
    }

    public void OnDeath()
    {
        _isDead = true;
        GhostMesh.SetActive(false);
        EyesMesh.SetActive(true);
        _navAgent.SetDestination(SpawnPoint.position);


    }

}
